<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// I used this to get used to laravel.
Route::get('/test', 'FeedController@test');

// Imports XML feed.
Route::get('/import', 'FeedController@import');

// Displays imported vehicles.
Route::get('/', 'FeedController@index');

// Handles API calls.
Route::get('/api', 'FeedController@api');

