<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

class FeedController extends Controller {

  // Callback for homepage.
  public function index() {
    $vehicles = DB::table('vehicle')->get();
    return view('index', compact('vehicles'));
  }

  //Callback for import page.
  public function import() {
    $insert_count = 0;
    // Given more time I would validate this feed both exists and is valid.
    $data = $this->importXML('http://youngwolf0.com/testfeed.xml');

    // Here I truncate the db before insertion to prevent duplication.
    // Given more time I would write something to prevent duplicate entries.
    // DB::table('vehicle')->truncate();
    // DB::table('vehicle')->insert($data);

    // I thought better of the truncate and added a check to see if licence plate
    // already exists in the db.
    foreach ($data as $vehicle) {
      $vehicle_exists = DB::table('vehicle')
        ->where('license_plate', '=', $vehicle['license_plate'])
        ->get();
      if (!count($vehicle_exists)) {
        $inserted = DB::table('vehicle')->insert($vehicle);
        if ($inserted) {
          $insert_count++;
        }
      }
    }
    return view('import', compact('insert_count'));
  }

  // Imports vehicle xml feed into array.
  private function importXML($feedUrl) {
    $xmlDoc = new \DOMDocument();
    $xmlDoc->load($feedUrl);
    $elements = $xmlDoc->getElementsByTagName('Vehicle');
    $data = [];
    // Loop elements in xml feed and add them to data array
    foreach ($elements as $node) {
      $temp = [];
      // These fields were stored as attributes, I want to store them alongside
      // The other data so I include them in the data array.
      $temp['manufacturer'] = $node->getAttribute('manufacturer');
      $temp['model'] = $node->getAttribute('model');

      foreach ($node->childNodes as $child) {
        // Fix data types for ints and bools.
        if (is_numeric($child->nodeValue)) {
          $value = (int) $child->nodeValue;
        }
        elseif (strtolower($child->nodeValue) == 'true') {
          $value = 1;
        }
        elseif (strtolower($child->nodeValue) == 'false') {
          $value = 0;
        }
        elseif ($child->nodeValue == '') {
          $value = 0;
        }
        else {
          $value = $child->nodeValue;
        }
        $temp[$child->nodeName] = $value;
      }
      $data[] = $temp;
    }

    return $data;
  }

  // This was just me learning laravell
  public function test() {
    $test = 'My test string';
    $options = [
      'Tom',
      'Dick',
      'Anne',
      'Harry',
    ];
    return view('test', compact('test', 'options'));
  }

  // Callback for API.
  public function api() {
    $query_params = Input::all();

    $query = DB::table('vehicle');

    if (isset($query_params['manufacturer'])) {
      $query->where('manufacturer', '=', $query_params['manufacturer']);
    }

    if (isset($query_params['model'])) {
      $query->where('model', '=', $query_params['model']);
    }

    $vehicles = $query->get();

    return $vehicles;
  }
}


