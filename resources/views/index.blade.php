<!DOCTYPE html>

<html>
<head>
</head>
<body>
<p>

<ul>
    <li><a href="/">Home</a></li>
    <li><a href="/import">Import Feed</a></li>
    <li><a href="/api">API</a></li>
</ul>

<h1>Homepage</h1>

<a>In the above menu you can click on import to pull in any new items in the xml feed, or you can go to the api, there you can either see the complete list of vehicles in JSON or you can filter using the query string paramiters "manufacturer" or "model" to filter down the list a little, for example: <a href="/api?manufacturer=Citroen">http://localhost:8000/api?manufacturer=Citroen</a></p>


<table summary="List of vehicles previously imported into site.">
    <caption>Previously imported vehicles</caption>
    <thead>
    <tr><th>Manufacturer</th><th>Model</th><th>Type</th><th>Usage</th><th>License Plate</th><th>Weight Category</th><th>No. Seats</th><th>Has Boot</th><th>Has Trailer</th><th>Owner Name</th><th>Owner Company</th><th>Owner Profession</th><th>Transmission</th><th>Colour</th><th>Is HGV</th><th>No. Doors</th><th>Sunroof</th><th>Has GPS</th><th>No. Wheels</th><th>Engine CC</th><th>Fuel Type</th></tr>
    </thead>
    <tbody>
        @foreach($vehicles as $vehicle)
            <tr>
                <td>{{$vehicle->manufacturer}}</td><td>{{$vehicle->model}}</td><td>{{$vehicle->type}}</td><td>{{$vehicle->usage}}</td><td>{{$vehicle->license_plate}}</td><td>{{$vehicle->weight_category}}</td><td>{{$vehicle->no_seats}}</td><td>{{$vehicle->has_boot}}</td><td>{{$vehicle->has_trailer}}</td><td>{{$vehicle->owner_name}}</td><td>{{$vehicle->owner_company}}</td><td>{{$vehicle->owner_profession}}</td><td>{{$vehicle->transmission}}</td><td>{{$vehicle->colour}}</td><td>{{$vehicle->is_hgv}}</td><td>{{$vehicle->no_doors}}</td><td>{{$vehicle->sunroof}}</td><td>{{$vehicle->has_gps}}</td><td>{{$vehicle->no_wheels}}</td><td>{{$vehicle->engine_cc}}</td><td>{{$vehicle->fuel_type}}</td>
            </tr>
        @endforeach
    </tbody>
</table>

</body>
</html>